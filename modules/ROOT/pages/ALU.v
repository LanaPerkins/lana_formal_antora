`include "CONFIG.v"

module ALU #(parameter BITWIDTH = `BITWIDTH)
(
    input iCLK,
    input iRST,
    // Opcode for Operation
    input [7:0] opcode,
    input [7:0] funct3,
    input [7:0] funct7,
    // Pair of inputs
    input [BITWIDTH  - 1:0] iA,
    input [BITWIDTH - 1:0] iB,
    // Full Adder
    input iCIN,
    output [BITWIDTH - 1:0] oFASUM,
    output oCOUT,
    // Adder
    output [BITWIDTH - 1:0] oSUM,
    // Subtracter
    output [BITWIDTH - 1:0] oDifference,
    // XOR
    output [BITWIDTH - 1:0] oXOR,
    // OR
    output [BITWIDTH - 1:0] oOR,
    // AND
    output [BITWIDTH - 1:0] oAND,
    // Multiplier
    output [BITWIDTH * 2:0] oPROD
);

// ---------------------------------------------------------------------------

// Adder
reg [BITWIDTH - 1:0] oSUM_q;
always@(posedge iCLK or negedge iRST) begin
    
    if (~iRST) begin
        oSUM_q <= 0;
    end
    
    else if (opcode == 0110011 && funct3 == 00000000 && funct7 == 00000000) begin
        oSUM_q <= iA + iB;
    end
    
    else begin
        oSUM_q <= oSUM_q;
    end
    
end

assign oSUM = oSUM_q;

// ---------------------------------------------------------------------------

// Subtracter
reg [BITWIDTH - 1:0] oDifference_q;
always@(posedge iCLK or negedge iRST) begin
    
    if (~iRST) begin
        oDifference_q <= 0;
    end
    
    else if (opcode == 0110011 && funct3 == 00000000 && funct7 == 00100000) begin
        oSUM_q <= iA - iB;
    end
    
    else begin
        oDifference_q <= oDifference_q;
    end
    
end

assign oDifference = oDifference_q;

// ---------------------------------------------------------------------------

// XOR
reg [BITWIDTH - 1:0] oXOR_q;
always@(posedge iCLK or negedge iRST) begin
    
    if (~iRST) begin
        oDifference_q <= 0;
    end
    
    else if (opcode == 0110011 && funct3 == 01000000 && funct7 == 00000000) begin
        oXOR_q <= iA ^ iB;
    end
    
    else begin
        oXOR_q <= oXOR_q;
    end
    
end

assign oXOR = oXOR_q;

// ---------------------------------------------------------------------------

// OR
reg [BITWIDTH - 1:0] oOR_q;
always@(posedge iCLK or negedge iRST) begin
    
    if (~iRST) begin
        oOR_q <= 0;
    end
    
    else if (opcode == 0110011 && funct3 == 01100000 && funct7 == 00000000) begin
        oOR_q <= iA | iB;
    end
    
    else begin
        oOR_q <= oOR_q;
    end
    
end

assign oOR = oOR_q;

// ---------------------------------------------------------------------------

// AND
reg [BITWIDTH - 1:0] oAND_q;
always@(posedge iCLK or negedge iRST) begin
    
    if (~iRST) begin
        oAND_q <= 0;
    end
    
    else if (opcode == 0110011 && funct3 == 01110000 && funct7 == 00000000) begin
        oAND_q <= iA & iB;
    end
    
    else begin
        oAND_q <= oAND_q;
    end
    
end

assign oAND = oAND_q;

// ---------------------------------------------------------------------------

// Full Adder
reg [BITWIDTH - 1:0] oFASUM_q;
reg oCOUT_q;
always@(posedge iCLK or negedge iRST) begin
    
    if (~iRST) begin      
        oFASUM_q <= 0;
        oCOUT_q <= 0;
    end

    else begin
        oFASUM_q <= iA ^ iB ^ iCIN;
        oCOUT_q <= (iA & iB) | (iB & iCIN) | (iA & iCIN);
    end

end

assign oFASUM = oFASUM_q;
assign oCOUT = oCOUT_q;

// ---------------------------------------------------------------------------

// Multiplier
reg [BITWIDTH*2 - 1:0] oPROD_q;
always@(posedge iCLK or negedge iRST) begin

    if (~iRST) begin
        oPROD_q <= 0;
    end
    
    else begin
        oPROD_q <= iA * iB;
    end
    
end

assign oPROD = oPROD_q;

endmodule