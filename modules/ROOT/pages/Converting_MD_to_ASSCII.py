#This code converts all items in this directory that is a md file to a adoc file ussing pandoc
#Your new file will appear in the same filepath

import subprocess
from pathlib import Path
from sys import path_hooks
from pdf2docx import Converter
import re
import string
import os

root="."

def convert_markdown_to_adoc(markdown_file, adoc_file):
    subprocess.run(["pandoc", "-t", "asciidoc", markdown_file  , "-o", adoc_file])

for path in Path(root).glob("**/*.md"):
    print(path)
    md_ascii = r"{Put your directory where your markdown file originates here}{}".format(path)
    path2, blank2, blank2=md_ascii.partition(".md")
    renamed = "{}.adoc".format(path)
    renamed2 = "{}.adoc".format(path2)
    convert_markdown_to_adoc(path, renamed)
    os.rename(renamed,renamed2)