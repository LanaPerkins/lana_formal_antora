from langchain_community.llms import HuggingFacePipeline
from transformers import AutoModelForCausalLM, AutoTokenizer, pipeline

from langchain_community.document_loaders import DirectoryLoader, TextLoader
from langchain.prompts import PromptTemplate
from langchain.document_loaders import PyPDFLoader
from langchain_core.output_parsers import StrOutputParser
# Loading Info from Directory


loader = DirectoryLoader('documents/', glob="**", show_progress=True, use_multithreading=True, loader_cls=TextLoader)
docs = loader.load_and_split()
print(len(docs))


model_path = "/home/draco/text-generation-webui/models/silverliningeda_llama-2-7b-silverliningeda-verilog-codegen"

model = AutoModelForCausalLM.from_pretrained(model_path, device_map="auto", offload_folder="offload")
#model = AutoModelForCausalLM.from_pretrained(model_path, offload_folder="offload")
tokenizer = AutoTokenizer.from_pretrained(model_path)
pipe = pipeline(
    "text-generation", model=model, tokenizer=tokenizer, max_new_tokens=100
)
llm = HuggingFacePipeline(pipeline=pipe)
prompt = PromptTemplate.from_template(
    "Summarize the main themes in these retrieved docs: {docs}"
)
#prompt = "Say something"
prompt = PromptTemplate.from_template(template)
chain = prompt | llm

question = "Write me an Adder in Verilog"

print(chain.invoke({"question": question}))
#docs = vectorstore.similarity_search(question)
#chain.invoke(docs)

#print(llm(prompt))

#this code runs models on gpu and obtains documents. this is still in
# progress no bueno atm look at script2 
# All this stuff in venv2 
# Michael, so you know for now I just added your model  